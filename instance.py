#!/usr/bin/env python
from PIL import Image
from skimage.measure import regionprops
import numpy as np
import cv2
import os
import time

dataset_path = './data_semantics'
image_path = os.path.join(dataset_path, 'image_2')
label_path = os.path.join(dataset_path, 'instance')

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.namedWindow('instance', cv2.WINDOW_NORMAL)

for f in os.listdir(image_path):
  start_time = time.time()

  img = cv2.imread(os.path.join(image_path, f))
  im = Image.open(os.path.join(label_path, f))
  in_data = np.asarray(im).copy()

  # remove objects other than cars
  idx = np.where(np.logical_or(in_data<6600, in_data>=6700))
  in_data[idx] = 0
  in_vector = np.zeros((in_data.shape[0], in_data.shape[1], 2), dtype=np.float32)

  # calculate the centroid of every car instance
  centers = []
  for i in regionprops(in_data):
    cy, cx = map(lambda x: int(x), i.centroid)
    centers.append((cx, cy))
    for y, x in i.coords:
      in_vector[y, x, 0] = cx - x
      in_vector[y, x, 1] = cy - y

  # calculate degrees and magnitude from displacement vector
  deg = np.arctan2(in_vector[:,:,1], in_vector[:,:,0]) * 180 / np.pi
  mag = np.sqrt(in_vector[:,:,0]**2 + in_vector[:,:,1]**2)
  mag = (mag - np.min(mag)) / (np.max(mag) - np.min(mag)) * 255

  in_img = np.zeros(img.shape, dtype=np.uint8)
  # hue (orientation)
  hue = (deg + 180) / 360 * 255
  in_img[:,:,0] = hue
  # saturation (magnitude)
  in_img[:,:,1] = mag
  # value must be 100
  in_img[:,:,2] = 255

  for c in centers:
    cv2.circle(img, c, color=(0,0,255), radius=3, thickness=-1)

  print("%s seconds" % (time.time() - start_time))
  cv2.imshow("image", img)
  cv2.resizeWindow("image", 900,300)
  in_img = cv2.cvtColor(in_img, cv2.COLOR_HSV2BGR)
  cv2.imshow("instance", in_img)
  cv2.resizeWindow("instance", 900,300)
  k = cv2.waitKey(0)
  # quit when 'q' is pressed
  if k == ord('q'):
    break

